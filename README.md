# vim

Install and configure vim

## Role Variables

* `bundles`
  * Type: List
  * Usages: Vim bundles to install

* `options`
  * Type: List
  * Usages: Vim options

* `remaps`
  * Type: List
  * Usages: Vim remaps

```yaml
vim:
  bundles:
    - name: 'faith/vim-go'
      opts: "'for': 'go'"
      config: |
        let g:go_fmt_fail_silentsly = 1
        let g:go_list_type = 'quickfix'
  options:
    - comment: '" Enable utf-8'
      config: 'set encoding=utf-8'
  remaps:
    - comment: '" Remove selection'
      config: 'vmap r "_d'
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.vim

## License

MIT
